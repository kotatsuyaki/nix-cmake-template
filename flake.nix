{
  inputs = {
    nixpkgs.url = github:nixos/nixpkgs/nixos-21.11;
    flake-utils.url = github:numtide/flake-utils;
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        packages.hello = pkgs.hello;

        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            clang-tools
            clang_13
            llvm_13
            llvmPackages_13.libstdcxxClang
            bear
            gnumake
            cmake
            boost
          ];
        };
      });
}

